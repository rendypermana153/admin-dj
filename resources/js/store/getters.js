import * as types from './types'

export default {
    [types.DESTINATION]: state => {
        return state.destination
    },

  	[types.PROTECTION_TYPE]: state => {
        return state.protection_type
    },

    [types.PERIOD]: state => {
        return state.period
    },

    [types.START_DATE]: state => {
        return state.start_date
    },

    [types.FORMATTED_START_DATE]: state => {
        return moment(state.start_date).format('DD-MM-YYYY')
    },

    [types.YEARS_BEFORE_DEPARTURE]: (state) => (years) => {
        return moment(state.start_date).subtract(years, 'years')
    },

    [types.DAYS_BEFORE_DEPARTURE]: (state) => (days) => {
        return moment(state.start_date).subtract(days, 'days')
    },

    [types.END_DATE]: state => {
        return state.end_date
    },

    [types.FORMATTED_END_DATE]: state => {
        return moment(state.end_date).format('DD-MM-YYYY')
    },

    [types.BIRTH_DATE]: state => {
        return state.birth_date
    },

    [types.BENEFIT_TYPE]: state => {
        return state.benefit_type
    },

    [types.NAME]: state => {
        return state.name
    },

    [types.PHONE]: state => {
        return state.phone
    },

    [types.EMAIL]: state => {
        return state.email
    },

    [types.GENDER]: state => {
        return state.gender
    },

    [types.NATIONALITY]: state => {
        return state.nationality
    },

    [types.PROVINCES]: state => {
        return state.provinces
    },

    [types.PROVINCE]: state => {
        return state.province
    },

    [types.CITY]: state => {
        return state.city
    },

    [types.ADDRESS]: state => {
        return state.address
    },

    [types.CURRENCY]: state => {
        return state.currency
    },

    [types.MEMBER_NAME]: state => {
        return state.member_name
    },

    [types.MEMBER_PHONE]: state => {
        return state.member_phone
    },

    [types.MEMBER_EMAIL]: state => {
        return state.member_email
    },

    [types.ID_NUMBER]: state => {
        return state.id_number
    },

    [types.PASSPORT_NUMBER]: state => {
        return state.passport_number
    },

    [types.DESTINATION_COUNTRY]: state => {
        return state.destination_country
    },

    [types.COUNTRIES]: state => {
        return state.countries
    },

    [types.DESTINATION_COUNTRIES_NAME]: state => {
        // return state.destination_country.map((c) => c.country_name)
        return state.destination_country
    },

    [types.FAMILIES]: state => {
        // return state.destination_country.map((c) => c.country_name)
        return state.families
    },

    [types.AVAILABLE_PACKAGES]: state => {
        // return state.destination_country.map((c) => c.country_name)
        return state.available_packages
    }

      
};
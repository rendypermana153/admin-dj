import * as types from './types';

export default {
    [types.UPDATE_DESTINATION]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_DESTINATION, payload)
    },
    [types.UPDATE_PROTECTION_TYPE]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_PROTECTION_TYPE, payload)
    },
    [types.UPDATE_PERIOD]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_PERIOD, payload)
    },
    [types.UPDATE_START_DATE]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_START_DATE, payload)
    },
    [types.UPDATE_END_DATE]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_END_DATE, payload)
    },
    [types.UPDATE_BIRTH_DATE]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_BIRTH_DATE, payload)
    },
    [types.UPDATE_BENEFIT_TYPE]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_BENEFIT_TYPE, payload)
    },
    [types.UPDATE_NAME]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_NAME, payload)
    },
    [types.UPDATE_PHONE]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_PHONE, payload)
    },
    [types.UPDATE_EMAIL]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_EMAIL, payload)
    },
    [types.UPDATE_GENDER]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_GENDER, payload)
    },
    [types.UPDATE_NATIONALITY]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_NATIONALITY, payload)
    },
    [types.UPDATE_PROVINCES]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_PROVINCES, payload)
    },
    [types.UPDATE_PROVINCE]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_PROVINCE, payload)
    },
    [types.UPDATE_CITY]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_CITY, payload)
    },
    [types.UPDATE_ADDRESS]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_ADDRESS, payload)
    },
    [types.UPDATE_CURRENCY]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_CURRENCY, payload)
    },
    [types.UPDATE_MEMBER_NAME]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_MEMBER_NAME, payload)
    },
    [types.UPDATE_MEMBER_PHONE]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_MEMBER_PHONE, payload)
    },
    [types.UPDATE_MEMBER_EMAIL]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_MEMBER_EMAIL, payload)
    },
    [types.UPDATE_ID_NUMBER]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_ID_NUMBER, payload)
    },
    
    [types.UPDATE_PASSPORT_NUMBER]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_PASSPORT_NUMBER, payload)
    },
    
    [types.UPDATE_DESTINATION_COUNTRY]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_DESTINATION_COUNTRY, payload)
    },

    [types.UPDATE_COUNTRIES]: ({commit}, payload) => {
        commit(types.MUTATE_UPDATE_COUNTRIES, payload)
    },

    [types.UPDATE_FAMILIES]: ({commit}, payload) => {
        commit(types.M_UPDATE_FAMILIES, payload)
    },

    [types.UPDATE_AVAILABLE_PACKAGES]: ({commit}, payload) => {
        commit(types.M_UPDATE_AVAILABLE_PACKAGES, payload)
    },

};
import Vue from 'vue';
import Vuex from 'vuex';

import actions from './actions';
import getters from './getters';
import mutations from './mutations';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
        destination: 'internasional',
        protection_type: 'individu',
        period: 'oneway',
        start_date: '',
        end_date: '',
        birth_date: '',
        benefit_type: '',
        name: '',
        phone: '',
        email: '',
        gender: 'male',
        province: '',
        city: '',
        address: '',
        nationality: 'wni',
        provinces: [],
        currency: 'usd',
        member_name: '',
        member_phone: '',
        member_email: '',
        id_number: '',
        passport_number: '',
        destination_country: [],
        countries: ['LOADING...'],
        families: [],
        available_packages: [],
    },
    getters,
    mutations,
    actions
});

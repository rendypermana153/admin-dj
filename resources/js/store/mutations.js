import * as types from './types';

export default {
    [types.MUTATE_UPDATE_DESTINATION]: (state, payload) => {
        state.destination = payload;
    },

    [types.MUTATE_UPDATE_PROTECTION_TYPE]: (state, payload) => {
        state.protection_type = payload;
    },

    [types.MUTATE_UPDATE_PERIOD]: (state, payload) => {
        state.period = payload;
    },

    [types.MUTATE_UPDATE_START_DATE]: (state, payload) => {
        state.start_date = payload;
    },

    [types.MUTATE_UPDATE_END_DATE]: (state, payload) => {
        state.end_date = payload;
    },

    [types.MUTATE_UPDATE_BIRTH_DATE]: (state, payload) => {
        state.birth_date = payload;
    },

    [types.MUTATE_UPDATE_BENEFIT_TYPE]: (state, payload) => {
        state.benefit_type = payload;
    },

    [types.MUTATE_UPDATE_NAME]: (state, payload) => {
        state.name = payload;
    },

    [types.MUTATE_UPDATE_PHONE]: (state, payload) => {
        state.phone = payload;
    },

    [types.MUTATE_UPDATE_EMAIL]: (state, payload) => {
        state.email = payload;
    },

    [types.MUTATE_UPDATE_GENDER]: (state, payload) => {
        state.gender = payload;
    },

    [types.MUTATE_UPDATE_NATIONALITY]: (state, payload) => {
        state.nationality = payload;
    },

    [types.MUTATE_UPDATE_PROVINCES]: (state, payload) => {
        state.provinces = payload;
    },

    [types.MUTATE_UPDATE_PROVINCE]: (state, payload) => {
        state.province = payload;
    },

    [types.MUTATE_UPDATE_CITY]: (state, payload) => {
        state.city = payload;
    },

    [types.MUTATE_UPDATE_ADDRESS]: (state, payload) => {
        state.address = payload;
    },

    [types.MUTATE_UPDATE_CURRENCY]: (state, payload) => {
        state.currency = payload;
    },

    [types.MUTATE_UPDATE_MEMBER_NAME]: (state, payload) => {
        state.member_name = payload;
    },

    [types.MUTATE_UPDATE_MEMBER_PHONE]: (state, payload) => {
        state.member_phone = payload;
    },

    [types.MUTATE_UPDATE_MEMBER_EMAIL]: (state, payload) => {
        state.member_email = payload;
    },

    [types.MUTATE_UPDATE_ID_NUMBER]: (state, payload) => {
        state.id_number = payload;
    },

    [types.MUTATE_UPDATE_PASSPORT_NUMBER]: (state, payload) => {
        state.passport_number = payload;
    },

    [types.MUTATE_UPDATE_DESTINATION_COUNTRY]: (state, payload) => {
        state.destination_country = payload;
    },

    [types.MUTATE_UPDATE_COUNTRIES]: (state, payload) => {
        state.countries = payload;
    },

    [types.M_UPDATE_FAMILIES]: (state, payload) => {
        state.families = payload;
    },

    [types.M_UPDATE_AVAILABLE_PACKAGES]: (state, payload) => {
        state.available_packages = payload;
    },

    
};
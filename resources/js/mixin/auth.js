export const auth = {
    data () {
        return {
            // token: this.$cookie.get('token')
            token: this.$cookie.get('token')
        }
    },
    // created () {
    //     this.authentication()
    // },
    methods: {
        authentication () {
            // console.log('authentication')
            if(!this.token || this.token != ''){
                // console.log('not login')
                this.$router.push({'name': 'Dashboard'})
            }
        }
    },
    mounted () {
        if(!this.token || this.token === ''){
            // this.token = token
            this.$router.push({'name': 'login'})
        }
    }
}
import Vue from 'vue';
import VueRouter from 'vue-router';

import Home from '@/js/components/ExampleComponent'

import DashView from '@/js/components/layout/DashboardContent'
import LayoutPage from '@/js/components/layout/LayoutPage'

//view
import Dashboard from '@/js/components/pages/Dashboard'
import NotFound from '@/js/components/pages/NotFound'
import LoginPage from '@/js/components/pages/Login'
import UserIndex from '@/js/components/pages/UserIndex'
import ProfileWalkot from '@/js/components/pages/ProfileWalkot'
import History from '@/js/components/pages/History'
import Information from '@/js/components/pages/Information'

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        { 
            path: '*', redirect: '/login' 
        },
        {
            path: '/login',
            name: 'login',
            component: LoginPage
        },
        {
            path:'/',
            component: LayoutPage,
            redirect:'/home',
            children: [
                {
                    path: 'home',
                    component: Dashboard,
                    name: 'Dashboard',
                    meta: {description: 'Overview of environment'}
                },
                {
                    path: 'user-index',
                    component: UserIndex,
                    name: 'UserIndex',
                    meta: {description: 'Overview of user'}
                },
                {
                    path: 'profile-walkot',
                    component: ProfileWalkot,
                    name: 'ProfileWalkot',
                    meta: {description: 'Overview of profile walkot'}
                },
                {
                    path: 'history-walkot',
                    component: History,
                    name: 'History',
                    meta: {description: 'Overview of history walkot'}
                },
                {
                    path: 'information',
                    component: Information,
                    name: 'Information',
                    meta: {description: 'Overview of information'}
                },
                { 
                    path: '*', redirect: '/home' 
                },
                {
                    path: 'not-found',
                    component: NotFound,
                    name: 'NotFound'
                }
            ]

        }
        
    ],
    linkActiveClass: "active"
});

export default router;
import Vue from 'vue'
import Router from 'vue-router'
import LayoutApp from '@/pages/layout/LayoutApp'
import HomePage from '@/pages/HomePage'
import EventPage from '@/pages/EventPage'
import FeedPage from '@/pages/FeedPage'
import RewardsPage from '@/pages/RewardsPage'
import ProfilePage from '@/pages/ProfilePage'
import DetailRewards from '@/pages/DetailRewards'
import BuktiTransaksi from '@/pages/BuktiTransaksi'
import HotPromo from '@/pages/HotPromo'
import LoginPage from '@/pages/LoginPage'
import Leaderboard from '@/pages/Leaderboard'
import PromotionDetail from '@/pages/PromotionDetail'
import KatalogPage from '@/pages/KatalogPage'
import RedemtionPage from '@/pages/RedemtionPage'
import SignUp from '@/pages/SignUp'
import ProfileEdit from '@/pages/ProfileEdit'
import PhotoUpload from '@/pages/PhotoUpload'
import LayoutLogin from '@/pages/LayoutLogin'
import PasswordForgot from '@/pages/PasswordForgot'
import TermService from '@/pages/TermService'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/login',
      name: 'LayoutLogin',
      component: LayoutLogin,
      redirect: '/login',
      children:[
        {
          path: '/login',
          name: 'login',
          component: LoginPage
        },
        {
          path: '/signup',
          name: 'signup',
          component: SignUp
        },
        {
          path: '/password-forgot',
          name: 'password-forgot',
          component: PasswordForgot
        }
      ]
    },
    { 
      path: '*', redirect: '/login' 
    },
    // {
    //   path: '/login',
    //   name: 'login',
    //   component: LoginPage
    // },
    {
      path: '/',
      redirect: {
          name: 'login'
      }
    },
    {
      path: '/event',
      name: 'event',
      component: EventPage
    },
    {
      path: '/bukti-transaksi',
      name: 'bukti-transaksi',
      component: RedemtionPage
    },
    {
      path: '/katalog',
      name: 'katalog',
      component: KatalogPage
    },
    {
      path: '/notification',
      name: 'notification',
      component: BuktiTransaksi
    },
    {
      path: '/hot-promo',
      name: 'hot-promo',
      component: HotPromo
    },
    {
      path: '/leaderboard',
      name: 'leaderboard',
      component: Leaderboard
    },
    {
      path: 'promo-detail/:id_promo',
      name: 'promo-detail',
      component: PromotionDetail
    },
    // {
    //   path: '/signup',
    //   name: 'signup',
    //   component: SignUp
    // },
    {
      path: '/edit-profile',
      name: 'edit-profile',
      component: ProfileEdit
    },
    {
      path: '/photo-upload',
      name: 'photo-upload',
      component: PhotoUpload
    },
    {
      path: '/',
      name: 'LayoutApp',
      component: LayoutApp,
      redirect: '/home',
      children: [
        {
          path: 'home',
          name: 'home',
          component: HomePage
        },
        {
          path: 'feed',
          name: 'feed',
          component: FeedPage
        },
        {
          path: 'rewards',
          name: 'rewards',
          component: RewardsPage
        },
        {
          path: 'profile',
          name: 'profile',
          component: ProfilePage
        },
        {
          path: 'detail-rewards/:id_voucher',
          name: 'detail-rewards',
          component: DetailRewards
        },
        {
          path: 'term-service',
          name: 'term-service',
          component: TermService
        }
      ]
    } 
  ]
})

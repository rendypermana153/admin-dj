/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');

// window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

//Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// const app = new Vue({
//     el: '#app',
// });

import './bootstrap';
import Vue from 'vue';
import BootstrapVue from 'bootstrap-vue';

import CKEditor from '@ckeditor/ckeditor5-vue';
Vue.use( CKEditor );

import VueCookie from 'vue-cookie'
Vue.use(VueCookie )

import Routes from '@/js/routes.js';
//import { VuejsDatatableFactory } from 'vuejs-datatable';
import App from '@/js/views/App'
import VueSweetalert2 from 'vue-sweetalert2';
const options = {
    confirmButtonColor: '#41b882',
    cancelButtonColor: '#ff7674',
};
// If you don't need the styles, do not connect
import 'sweetalert2/dist/sweetalert2.min.css';
 
Vue.use(VueSweetalert2, options)
// Vue.use( VuejsDatatableFactory );
Vue.use(BootstrapVue)

const app = new Vue({
    el: '#app',
    router: Routes,
    render: h => h(App),
});

export default app;


<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class SinglePageController extends Controller
{
    public function index() {
        return view('app');
    }

    public function login() {
        return view('login');
        
    }
}

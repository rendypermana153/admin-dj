<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use URL;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class FungsiController extends Controller
{
    public function update_profile(Request $request) {
        //return $request->all();
        // $file = $request['image'];
        // $filename = $file->getClientOriginalName();
        // $filext= $file->getClientOriginalExtension();
        // Storage::disk('public')->put('profile_walkot',$file);

            $file = $request['image'];
            // $url = 'http://localhost:8000';
            $url = url('/');
            $file = $request->file('image');
            $destination_path = public_path('/profile_walkot/');
            $filename = str_random(6).'_'.$file->getClientOriginalName();
            $file->move($destination_path, $filename);
            $url_image = $url . '/profile_walkot/' . $filename;
            
            return response()->json([
                'status'    => true,
                'image_url'    => $url_image
            ]);
        
        // return with([
        //     'image_url' => $url_image,
        //     'status' => 'success'
        // ]);

         
    }
}
